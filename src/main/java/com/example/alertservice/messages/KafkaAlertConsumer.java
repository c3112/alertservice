package com.example.alertservice.messages;

import com.example.alertservice.Mailer.Mail;
import com.example.alertservice.Mailer.MailService;
import com.example.alertservice.controllers.AlertController;
import com.example.alertservice.controllers.RestService;
import com.example.alertservice.models.Alert;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

@Component
public class KafkaAlertConsumer {

    @Autowired
    AlertController alertController = new AlertController();

    Logger LOG = LoggerFactory.getLogger(KafkaAlertConsumer.class);

    RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();

    @Autowired
    RestService restService = new RestService(restTemplateBuilder);

    @Autowired
    MailService mailService;

    Mail mail = new Mail();


    @KafkaListener(id = "alert", topics = "topic-alert")
    public void listenerAlert(String data) throws ParseException {
        mail.setMailFrom("covidapp.mtp@gmail.com");
        LOG.info(data);
        Map alertMap = jsonToMap(data);
        String username = (String) alertMap.get("username");
        double latitude = (double) alertMap.get("latitude");
        double longitude = (double) alertMap.get("longitude");
        String timestampString = (String) alertMap.get("timestamp");
        //DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM dd, yyyy, h:mm:ss a");
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy, hh:mm:ss a", Locale.ENGLISH);
        Date date = sdf.parse(timestampString);

        String response = restService.getUserInfoWithUsername(username);
        System.out.println("------------------user response-----------------");
        System.out.println(response);
        Map userMap = jsonToMap(response);
        String email = (String) userMap.get("email");
        System.out.println("------------------mail address-----------------");
        System.out.println(email);
        System.out.println(("----------------------------------------------"));
        // Send mail to user
        mail.setMailTo(email);
        mail.setMailSubject("Alert COVID !");
        mail.setMailContent("Bonjour, il y a un cas positif pas loins de vous. L'heure du proximité : " + timestampString + " Pensez bien à vous faire tester en cas de symptômes évoquant le Covid-19");
        mailService.sendEmail(mail);

        // Save alert in db
        Alert alert = new Alert();
        alert.setUser_alerted(username);
        Date dateAlert = new Date();
        long time = dateAlert.getTime();
        Timestamp ts = new Timestamp(time);
        alert.setAlert_date(ts);
        alertController.create(alert);

    }

    public static Map jsonToMap(String data){
        Gson gson = new Gson();
        Map dataMap = gson.fromJson(data, Map.class);
        return dataMap;
    }


}
