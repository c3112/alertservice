package com.example.alertservice.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.Date;

@Entity(name="alert")
@Access(AccessType.FIELD)
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class Alert {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long alert_id;
    private String user_alerted;
    private Date alert_date;

    public void setAlert_id(Long alert_id) {
        this.alert_id = alert_id;
    }

    public void setUser_alerted(String user_alerted) {
        this.user_alerted = user_alerted;
    }

    public Long getAlert_id() {
        return alert_id;
    }

    public String getUser_alerted() {
        return user_alerted;
    }

    public Date getAlert_date() {
        return alert_date;
    }

    public void setAlert_date(Date alert_date) {
        this.alert_date = alert_date;
    }
}
