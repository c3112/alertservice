package com.example.alertservice.controllers;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class RestService {

    private final RestTemplate restTemplate;

    public RestService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public String getUserInfoWithUsername(String username) {
        String url = "http://localhost:5000/api/users/";
        return this.restTemplate.getForObject(url+username, String.class);
    }
}