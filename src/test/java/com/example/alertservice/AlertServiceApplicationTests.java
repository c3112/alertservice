package com.example.alertservice;

import com.example.alertservice.controllers.AlertController;
import com.example.alertservice.models.Alert;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;

@ContextConfiguration(classes = AlertServiceApplication.class)
@WebMvcTest(controllers = AlertController.class)
@AutoConfigureMockMvc
class AlertServiceApplicationTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AlertController alertController;

    private String asJsonString(Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void contextLoads(){ assertThat(alertController).isNotNull();}

    @Test
    @DisplayName("Test Get - Alert")
    public void testGetLocation() throws Exception{
        mockMvc.perform(get("/api/v1/alerts"))
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("Test Create Location")
    public void testControllerCreateLocation() throws Exception {
        Date date = new Date();
        long time = date.getTime();
        Timestamp ts = new Timestamp(time);
        Alert postAlert = new Alert();
        // Location mockLocation = new Location(0,43.6329382,3.8628207,ts);
        //doReturn(mockLocation).when(locationsController).create(mockLocation);

        /*mockMvc.perform(post("/api/v1/locations")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(postLocation)))
                .andExpect(status().isCreated())
                .andReturn().getResponse().getContentAsString();*/
    }


}
