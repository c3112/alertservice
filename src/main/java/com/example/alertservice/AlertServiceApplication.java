package com.example.alertservice;

import com.example.alertservice.Mailer.Mail;
import com.example.alertservice.Mailer.MailService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class AlertServiceApplication {

    public static void main(String[] args) {
        //Mail mail = new Mail();
        //mail.setMailFrom("jingjing.xiang1997@gmail.com");
        //mail.setMailTo("jingjing.xiang1997@gmail.com");
        //mail.setMailSubject("Spring Boot - Email Example");
        // mail.setMailContent("Learn How to send Email using Spring Boot!!!\n\nThanks\nwww.technicalkeeda.com");

        //ApplicationContext ctx = SpringApplication.run(AlertServiceApplication.class, args);
        // MailService mailService = (MailService) ctx.getBean("mailService");
        // mailService.sendEmail(mail);

        SpringApplication.run(AlertServiceApplication.class, args);
    }

}
