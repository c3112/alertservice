FROM openjdk:16-jdk-alpine
VOLUME /tmp
COPY build/libs/AlertService-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]