package com.example.alertservice.controllers;

import com.example.alertservice.models.Alert;
import com.example.alertservice.repositories.AlertRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/v1/alerts")
public class AlertController {
    @Autowired
    AlertRepository alertRepository;

    @GetMapping
    public List<Alert> list() {
        return alertRepository.findAll();
    }

    @GetMapping
    @RequestMapping("{id}")
    public Alert get(@PathVariable Long id) {
        if(alertRepository.findById(id).isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Alert with ID "+id+" not found");
        }
        return alertRepository.getById(id);
    }
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Alert create(@RequestBody final Alert alert) {
        return alertRepository.saveAndFlush(alert);
    }

    @RequestMapping(value = "{id}" , method = RequestMethod.DELETE)
    public void delete(@PathVariable Long id) {
        alertRepository.deleteById(id);
    }
}
